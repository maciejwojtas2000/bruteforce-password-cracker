#include<stdio.h>
#include<stdlib.h>
#include<string.h>
#include<openssl/md5.h>
#include<pthread.h>
#include<unistd.h>
#include<signal.h>
#include<ctype.h>
#define NUM_THREADS 4
char **dictionary;
char **hasla;
char **hasla2;

int flaga=0;
struct wyniki{
    char *wyraz[1000];
    char *hash[1000];
};
struct wyniki wynik1;

int ilehasel=0;
pthread_mutex_t block;
char *str2md5(const char *str, int length) 
{
    int n;
    MD5_CTX c;
    unsigned char digest[16];
    char *out = (char*)malloc(33);
    MD5_Init(&c);
    while (length > 0) {
        if (length > 512) {
            MD5_Update(&c, str, 512);
        } else {
            MD5_Update(&c, str, length);
        }
        length -= 512;
        str += 512;
    }
    MD5_Final(digest, &c);
    for (n = 0; n < 16; ++n) {
        snprintf(&(out[n*2]), 16*2, "%02x", (unsigned int)digest[n]);
    }
    return out;
} 
char **loadfile(char *filename)
    {
        FILE *f=fopen(filename, "r");
        if(!f)
        {
            fprintf(stderr,"Nie można otworzyć słownika");
        }
     
        int arrlen=0;
        char **lines=NULL;
        char buff[1000];
        int i=0;
        
        while(fgets(buff,1000,f))
            {   
                if(i==arrlen)
                    {   
                    arrlen+=1;

                    char ** newlines= (char **)realloc(lines,arrlen*sizeof(char*));
                    if(!newlines)
                        {  
                            fprintf(stderr,"Za mało pamięci");
                            exit(1);
                        }
                    lines=newlines;

                
            }
                if(buff[strlen(buff)]=='\n')
                {
                    buff[strlen(buff)]='\0';
                }
                int slen=strlen(buff);
                char *str=(char *)malloc((slen+1)*sizeof(char*));
                strcpy(str,buff);
                lines[i]=str;
                i++;
            }
           if(i==arrlen)
           {
               char **newarr=(char**)realloc(lines,(arrlen+1)*sizeof(char*));
                if(!newarr)
                {
                fprintf(stderr,"Nie można przealokować");
                exit(1);
                }
                lines=newarr;
           }
           lines[i]=NULL;
           return lines;
    }


void *producer0(){
    char pom3[50];
    char pom[50];
    char pom2[50];
    for(int p=0; dictionary[p]!=NULL;p++)
    {
        strcpy(pom, dictionary[p]);
    
        //printf("%s\n",pom);
            for(int j=0;pom[j]!=0;j++) // wszystkie litery małe
            {
                pom[j]=tolower(pom[j]);
            }

        strncpy(pom2, str2md5(pom, strlen(pom)),33);//zahashowanie slowa
     
        for(int k=0; hasla[k]!=NULL;k++)
        { //sprawdzenie hasha z lista hashów
       // printf("k\n");
       strncpy(pom3,str2md5(hasla[k], strlen(hasla[k])),33);
            if(!strcmp(pom2,pom3))
            {
    pthread_mutex_lock(&block);
                
                
                //strncpy(wynik1.wyraz[ilehasel],pom,35);
                
                wynik1.wyraz[ilehasel]=malloc(strlen(pom)+1);
                wynik1.hash[ilehasel]=malloc(strlen(pom2)+1);
                strcpy(wynik1.hash[ilehasel],pom2);
                strcpy(wynik1.wyraz[ilehasel],pom);
                printf("Znalezione przez wątek 0\n");
                printf("HASŁO NUMER: %d\n", ilehasel+1);
                printf("%s \n",pom2);
                printf("%s \n",pom);
                ilehasel++;
                flaga=1;

                pthread_mutex_unlock(&block);
                
            }
            
        }

    }


}

void *producer1()
{   char pom3[50];
    char pom[50];
    char pom2[50];
   
  for(int p=0; dictionary[p]!=NULL;p++)
    {
    
        strcpy(pom, dictionary[p]);

                pom[0]=toupper(pom[0]);

        strncpy(pom2, str2md5(pom, strlen(pom)),33);//zahashowanie slowa
     
        for(int k=0; hasla[k]!=NULL;k++)
        { //sprawdzenie hasha z lista hashów
       // printf("k\n");
       strncpy(pom3,str2md5(hasla[k], strlen(hasla[k])),33);
            if(!strcmp(pom2,pom3))
            {   

                pthread_mutex_lock(&block);
                
                //strncpy(wynik1.wyraz[ilehasel],pom,35);
                
                
                //strncpy(wynik1.wyraz[ilehasel],pom,35);
                
                wynik1.wyraz[ilehasel]=malloc(strlen(pom)+1);
                wynik1.hash[ilehasel]=malloc(strlen(pom2)+1);
                strcpy(wynik1.hash[ilehasel],pom2);
                strcpy(wynik1.wyraz[ilehasel],pom);
              printf("Znalezione przez wątek 1\n");
              printf("HASŁO NUMER: %d\n", ilehasel+1);
               printf("%s \n",pom2);
             printf("%s \n",pom);
                ilehasel++;
                flaga=2;

                pthread_mutex_unlock(&block);
                
            }
            
        }

    }


}
void *producer2()
{
   char pom3[50];
    char pom[50];
    char pom2[50];
  for(int p=0; dictionary[p]!=NULL;p++)
    {
        strcpy(pom, dictionary[p]);
    
        //printf("%s\n",pom);
            for(int j=0;j<strlen(pom);j++) // wszystkie litery duze
            {
                pom[j]=toupper(pom[j]);
            }

        strncpy(pom2, str2md5(pom, strlen(pom)),33);//zahashowanie slowa
     
        for(int k=0; hasla[k]!=NULL;k++)
        { //sprawdzenie hasha z lista hashów
       // printf("k\n");
       strncpy(pom3,str2md5(hasla[k], strlen(hasla[k])),33);
            if(!strcmp(pom2,pom3))
            {
                
                 pthread_mutex_lock(&block);
                //strncpy(wynik1.wyraz[ilehasel],pom,35);
                
                wynik1.wyraz[ilehasel]=malloc(strlen(pom)+1);
                wynik1.hash[ilehasel]=malloc(strlen(pom2)+1);
                strcpy(wynik1.hash[ilehasel],pom2);
                strcpy(wynik1.wyraz[ilehasel],pom);
             printf("Znalezione przez wątek 2\n");
                printf("HASŁO NUMER: %d", ilehasel+1);
                printf("%s \n",pom2);
                printf("%s \n",pom);
                ilehasel++;
                flaga=3;
                pthread_mutex_unlock(&block);
            }
            
        }

    }


}
void *konsument(){
    if(flaga>0){
                printf("Znalezione przez wątek %d\n",flaga-1);
                printf("HASŁO NUMER: %d\n", ilehasel+1);
                printf("Hasło: %s \n",wynik1.wyraz[ilehasel]);
                printf("Hash: S%s \n",wynik1.hash[ilehasel]);
               flaga=0;
              
    }
    
}
int main(int argc, char* argv[])
{ 
   
    pthread_t thread[NUM_THREADS];
    pthread_mutex_init(&block,NULL);

    if((argc=!3))
    {
        fprintf(stderr, "Niepoprawna ilośc plikóœ wejściowych");
        exit(1);
    }
    dictionary=loadfile(argv[1]);
    if(!dictionary)
    {
        fprintf(stderr, "Nie mogę załadować do słownika struktury \n");
        exit(1);
    }
    hasla=loadfile(argv[2]);
    if(!hasla)
    {
        fprintf(stderr,"Nie mogę załadować haseł do struktury");
    }
    

    pthread_create(&thread[0],NULL, &producer0, NULL);
    pthread_create(&thread[1],NULL, &producer1, NULL);
    pthread_create(&thread[2],NULL, &producer2, NULL);
    pthread_create(&thread[3],NULL, &konsument, NULL);
    pthread_join(thread[0],NULL);
    pthread_join(thread[1],NULL);
    pthread_join(thread[2],NULL);
    pthread_join(thread[3],NULL);
    pthread_cancel(thread[0]);
    pthread_cancel(thread[1]);
    pthread_cancel(thread[2]);
    pthread_cancel(thread[3]);
    pthread_mutex_destroy(&block);

    //producer0();

    printf("\n \n \n \n \n \n \n \n PODSUMOWANIE:\n");
    for(int r=0; r<3;r++)
    {
        printf("Nr hasla: %d \n haslo:%s \n hash: %s\n",r, wynik1.wyraz[r],wynik1.hash[r]);
    }


}
